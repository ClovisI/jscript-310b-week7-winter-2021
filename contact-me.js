const contactReason = document.getElementById('reason');
const jobRelated = document.getElementsByClassName('job-related');
const codeRelated= document.getElementsByClassName('code-related');

const name = document.getElementById("name");
const email = document.getElementById("email");
const form = document.getElementById('contact-form');
const regex = new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
const errorNameInputMessage = "you must enter a vaild name of 3 or more characters";
const errorEmailInputMessage = "you must enter a full, valid email address"
const contactErrorMessage= "Please select an option from the drop down menu";

const displayMessage = function(inputEl, message) {
  let errorEl = inputEl.parentElement.querySelector('.error');
  errorEl.innerText = message;
};

let isFormValid = false;

function validateName () {
  if(name.value==null || name.value=="" || name.value.length<=2){
    displayMessage(name, errorNameInputMessage);
    isFormValid = true;
  }
  else{
    displayMessage(name, "");
  }
}

function validateEmail () {
  if(email.value==null || email.value=="" || !regex.test(email.value)){
    displayMessage(email, errorEmailInputMessage);
    isFormValid = false;
  }
  else{
    displayMessage(email, "");
  }
}


form.addEventListener('submit',function(e){
  if(!isFormValid){e.preventDefault();}
  // if(isFormValid){
  //   validateName();
  //   validateEmail();
  // }
  validateName();
  validateEmail();

});

function nameValidation(name){
  if(name.value==null || name.value=="" || name.value<=2){
    displayMessage(name,errorNameInputMessage);
  }
  else{
    displayMessage(name,"");
  }
}

function emailValidation(email){
  if(email.value==null || email.value=="" || !regex.test(email.value)){
    displayMessage(email,errorEmailInputMessage);
  }
  else{
    displayMessage(email,"");
  }
}

contactReason.addEventListener("change",function(e){
  if(contactReason.selectedIndex===0){
    displayMessage(contactReason, contactErrorMessage)
  }
  else if (contactReason.selectedIndex===1) {
    displayMessage(contactReason, "");
    for (var i = 0; i < jobRelated.length; i++) {
      jobRelated[i].classList.remove('d-none')
    }
    for (var i = 0; i < codeRelated.length; i++) {
      codeRelated[i].classList.add('d-none')
    }
  }
  else{
    displayMessage(contactReason, "");
    for (var i = 0; i < codeRelated.length; i++) {
      codeRelated[i].classList.remove('d-none')
    }
    for (var i = 0; i < jobRelated.length; i++) {
      jobRelated[i].classList.add('d-none')
    }
  }
})
